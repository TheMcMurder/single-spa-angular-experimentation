const webpack = require('webpack');

module.exports = {
  plugins: [
    new webpack.DefinePlugin({
      "VERSION": JSON.stringify("4711"),
      "SINGLE_SPA": false,
    })
  ]
}
