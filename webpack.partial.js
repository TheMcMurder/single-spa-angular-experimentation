const webpack = require('webpack');

module.exports = {
  output: {
    libraryTarget: 'amd',
  },
  plugins: [
    new webpack.DefinePlugin({
      "VERSION": JSON.stringify("4711"),
      "SINGLE_SPA": true,
    })
  ]
}
