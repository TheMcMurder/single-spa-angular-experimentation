import 'core-js/es7/reflect';
import 'zone.js';
import singleSpaAngular from 'single-spa-angular';

import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { AppModule } from './app/app.module';
import { environment } from './environments/environment';
import { Router } from '@angular/router';

if (environment.production) {
  enableProdMode();
}

declare const SINGLE_SPA: boolean;

const ngLifecycles = singleSpaAngular({
  domElementGetter,
  mainModule: AppModule,
  angularPlatform: platformBrowserDynamic(),
  template: `<app-root />`,
  Router,
})

export const bootstrap = [
  ngLifecycles.bootstrap,
];

export const mount = [
  ngLifecycles.mount,
];

export const unmount = [
  ngLifecycles.unmount,
];


function domElementGetter() {
  return document.querySelector('#angular');
}

if (!SINGLE_SPA) {
  platformBrowserDynamic().bootstrapModule(AppModule)
    .catch(err => console.error(err));
}

