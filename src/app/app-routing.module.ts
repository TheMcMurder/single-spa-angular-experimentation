import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {APP_BASE_HREF} from '@angular/common';
import { Thing1Component } from './thing1/thing1.component'
import { Thing2Component } from './thing2/thing2.component'
import { DashboardComponent } from './dashboard/dashboard.component'

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
  },
  {
    path: '1',
    component: Thing1Component,
  },
  {
    path: '2',
    component: Thing2Component
  }
];

@NgModule({
  providers: [{provide: APP_BASE_HREF, useValue: '/angular'}],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
